Dieses Skript liest eine Liste von Diensten und Mailadressen ein.

Die eingelesenen Dienste werden auf Status 'running' überprüft.

Sollte der Dienst nicht laufen, werden die Dienste entsprechend dem Startmodus neugestartet, sowie Warn-Mails an die Mailadressen gesendet.

Anwendung:
```
#!

dienste.bat Dienstdatei Mailingliste [Logfile] [Startmodus=1/2/3/4] [Wartezeit]
```

In die Dienstdatei schreibt man einfach die Dienste in der Reihenfolge auf, in der sie neugestartet werden sollen (häng vom Startmodus ab).

In jede Zeile schreibt man einen einzigen Dienst.

Bei manchen Diensten muss man zur Bestätigung des Stoppvorgangs den Schalter '/y' übergeben (Bsp: fwsrv /y).

Für den Startvorgang der Dienste werden diese Schalter ignoriert.

Zeilen können mit vorangestelltem Semikolon ';' auskommentiert werden, diese Zeilen werden ignoriert (Bsp: ;Kommentar).


In die Mailingliste schreibt man einfach alle Mailadressen hinein, die bei Statusänderung eines Dienstes informiert werden sollen (Bsp: marwin.rieger@kl-it-consult.de).

In jede Zeile schreibt man eine einzige Mailadresse.

Zeilen können mit vorangestelltem Semikolon ';' auskommentiert werden, diese Zeilen werden ignoriert (Bsp: ;Kommentar).


In der Logfile werden folgende Aktivitäten dokumentiert:

* Wann das Skript ausgeführt wurde (Datum, Uhrzeit)
* Welche Dienste gestoppt wurden (Name + jeweilige Erfolgs- oder Fehlmeldungen)
* Welche Dienste gestartet wurden (Name + jeweilige Erfolgs- oder Fehlmeldungen)
* aufgetretene Statusänderung bei einem Dienst (Name des Dienstes, Datum, Uhrzeit, Status)

Der Standardparameter ist hier c:\dienste.bat_log.txt

Das Skript kann mit folgenden Startmodi ausgeführt werden:

* Startmodus 1 = Standard. Alle Dienste werden neugestartet, danach ueberprueft und ggf. alle neugestartet.
* Startmodus 2 = Die Dienste werden ueberprueft und ggf. alle neugestartet.
* Startmodus 3 = Alle Dienste werden neugestartet, danach ueberprueft und ggf. der einzelne Dienst neugestartet.
* Startmodus 4 = Die Dienste werden ueberprueft und ggf. der einzelne Dienst neugestartet.

Die Wartezeit gibt an, in welchem Zyklus die Dienste überprüft werden (Bsp: 120).

Standardmäßig ist die Wartezeit auf 120s gesetzt.

Entstanden im Rahmen meiner Ausbildung im Jahr 2012.