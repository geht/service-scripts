@echo off

:: ~~~~~~~~~~~~~~~~~~~A Script by Marwin Rieger~~~~~~~~~~~~~~~~~~~
:: ---------------------------------------------------------------
::Dieses Skript liest eine Liste von Diensten und Mailadressen ein.
::Die eingelesenen Dienste werden auf Status 'running' �berpr�ft.
::Sollte der Dienst nicht laufen, werden die Dienste entsprechend dem Startmodus neugestartet, sowie Warn-Mails an die Mailadressen gesendet.

::Anwendung:
::dienste.bat Dienstdatei Mailingliste [Logfile] [Startmodus=1/2/3/4] [Wartezeit]

::In die Dienstdatei schreibt man einfach die Dienste in der Reihenfolge auf, in der sie neugestartet werden sollen (h�ng vom Startmodus ab)
::In jede Zeile schreibt man einen einzigen Dienst.
::Bei manchen Diensten muss man zur Best�tigung des Stoppvorgangs den Schalter '/y' �bergeben (Bsp: fwsrv /y)
::f�r den Startvorgang der Dienste werden diese Schalter ignoriert
::Zeilen k�nnen mit vorangestelltem Semikolon ';' auskommentiert werden, diese Zeilen werden ignoriert (Bsp: ;Kommentar)

::In die Mailingliste schreibt man einfach alle Mailadressen hinein, die bei Status�nderung eines Dienstes informiert werden sollen (Bsp: marwin.rieger@kl-it-consult.de)
::In jede Zeile schreibt man eine einzige Mailadresse
::Zeilen k�nnen mit vorangestelltem Semikolon ';' auskommentiert werden, diese Zeilen werden ignoriert (Bsp: ;Kommentar)

::In der Logfile werden folgende Aktivit�ten dokumentiert:
:: - Wann das Skript ausgef�hrt wurde (Datum, Uhrzeit)
:: - Welche Dienste gestoppt wurden (Name + jeweilige Erfolgs- oder Fehlmeldungen)
:: - Welche Dienste gestartet wurden (Name + jeweilige Erfolgs- oder Fehlmeldungen)
:: - aufgetretene Status�nderung bei einem Dienst (Name des Dienstes, Datum, Uhrzeit, Status)
::Der Standardparameter ist hier c:\dienste.bat_log.txt

::Das Skript kann mit folgenden Startmodi ausgef�hrt werden:
:: - Startmodus 1 = Standard. Alle Dienste werden neugestartet, danach ueberprueft und ggf. alle neugestartet.
:: - Startmodus 2 = Die Dienste werden ueberprueft und ggf. alle neugestartet.
:: - Startmodus 3 = Alle Dienste werden neugestartet, danach ueberprueft und ggf. der einzelne Dienst neugestartet.
:: - Startmodus 4 = Die Dienste werden ueberprueft und ggf. der einzelne Dienst neugestartet.

::Die Wartezeit gibt an, in welchen Zyklus die Dienste �berpr�ft werden (Bsp: 120)
::Standardm��ig ist die Wartezeit auf 120s gesetzt.
:: ---------------------------------------------------------------


::Konstanten
set trenn1=~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set trenn2=--------------------------------------------------------------------
set trenn3=---------------------------------------------
set mailserver=192.168.0.52:25

::Ausgabe von Standardinformationen, z.B. wie das Script zu verwenden ist.
echo usage: %0 dienstdatei mailingliste [logdatei] [startmodus=1/2/3/4] [wartezeit]
echo.
echo Startmodus 1 = Standard. Alle Dienste werden neugestartet, danach ueberprueft und ggf. alle neugestartet.
echo Startmodus 2 = Die Dienste werden ueberprueft und ggf. alle neugestartet.
echo Startmodus 3 = Alle Dienste werden neugestartet, danach ueberprueft und ggf. der einzelne Dienst neugestartet.
echo Startmodus 4 = Die Dienste werden ueberprueft und ggf. der einzelne Dienst neugestartet.
echo.
echo Beispiele:
echo %0 dienste.txt mail.txt
echo %0 dienste.txt mail.txt log.txt
echo %0 dienste.txt mail.txt log.txt 4 60
echo.
echo Standardparameter
echo logdatei = c:\%0_log.txt
echo startmodus = 1
echo wartezeit = 120
echo.


::Wenn keine Dienstdatei �bergeben wurde, dann abbrechen, ansonsten Parameter verwenden
if %1!==! (
	goto ENDE
	) else (
	set input=%1
)

::Wenn keine Mailingliste �bergeben wurde, dann abbrechen, ansonsten Parameter verwenden
if %2!==! (
	goto ENDE
	) else (
	set mailingliste=%2
)

::Wenn keine Logdatei �bergeben wurde, fragen, ob Standardparameter verwendet werden sollen
::Wenn ja, dann Standardparameter verwenden, wenn nein, dann abrechen
::Nach 30 Sekunden wird die Frage automatisch mit ja beantwortet
if %3!==! (
	choice /t 30 /D J /M "Standardparameter verwenden?"
	if errorlevel 2 (
		goto ENDE
		) else (
		set log=c:\%0_log.txt
		)
	) else (
	set log=%3
)

::Wenn kein Startmodus �bergeben wurde, Standardparameter 1 verwenden, ansonsten Parameter verwenden
if %4!==! (
	set modus=1
) else (
	set modus=%4
)

::Wenn keine Wartezeit �bergeben wurde, Standardparameter 120 verwenden, ansonsten Parameter verwenden
if %5!==! (
	set wartezeit=120
) else (
	set wartezeit=%5
)

::Ausgabe der �bermittelten Parameter
echo Uebergebene Parameter
echo dienstdatei = %input%
echo mailingliste = %mailingliste%
echo logdatei = %log%
echo startmodus = %modus%
echo wartezeit = %wartezeit%
echo.

::Programmstart, je nach �bermitteltem Startmodus
if %modus%==1 (
call :ALLENEUSTARTEN
call :PRUEFEN
)
if %modus%==2 (
call :PRUEFEN
)
if %modus%==3 (
call :ALLENEUSTARTEN
call :PRUEFEN2
)
if %modus%==4 (
call :PRUEFEN2
)
goto ENDE


::Sprungmarke ALLENEUSTARTEN, alle Dienste werden in der vorgegebenen Reihenfolge neugestartet
:ALLENEUSTARTEN
call :PRINTKOPF
call :PRINTSTOP

::Dienste werde gestoppt (Schalter wie z.B. /y werden mit ausgelesen)
for /F "eol=; tokens=*" %%l in (%input%) do (
call :STOPSERVICE %%l
)

call :PRINTSTART

::Dienste werden gestartet (Schalter werden ignoriert, da z.B /y als Best�tigung nicht ben�tigt wird)
for /F "eol=; tokens=1" %%l in (%input%) do (
call :STARTSERVICE %%l
)
goto :EOF


::Sprungmarke PRUEFEN, die Dienste werden �berpr�ft und ggf. alle in der vorgegebenen Reihenfolge neugestartet
:PRUEFEN

call :WARTEN
echo Dienste werden ueberprueft...
echo.

::Dienste werden �berpr�ft (Schalter werden ignoriert)
for /F "eol=; tokens=1" %%l in (%input%) do (

sc query %%l | find /i "running">NUL
if errorlevel==1 (
call :PRINTERROR %%l
call :ALLENEUSTARTEN
call :MAIL %%l
)
)
goto PRUEFEN


::Sprungmarke PRUEFEN2, die Diesnte werden �berpr�ft und ggf. wird der Dienst, der gestoppt wurde wieder gestartet
:PRUEFEN2

call :WARTEN
echo Dienste werden ueberprueft...
echo.

::Dienste �berpr�fen (Schalter werden ignoriert)
for /F "eol=; tokens=1" %%l in (%input%) do (

sc query %%l | find /i "running">NUL
if errorlevel==1 (
call :PRINTERROR %%l
call :PRINTKOPF
call :PRINTSTART
call :STARTSERVICE %%l
call :MAIL %%l
)
)
goto PRUEFEN2


::Sprungmarke PRINTKOPF, der Kopf der log wird geschrieben
:PRINTKOPF
::Print in Log
echo. 1>> %log% 2>>&1
echo %trenn1% 1>> %log% 2>>&1
echo. 1>> %log% 2>>&1
echo Skript wurde ausgefuehrt am:  1>> %log% 2>>&1
date /t 1>> %log% 2>>&1
time /t 1>> %log% 2>>&1

::Print auf Konsole
echo.
echo %trenn1%
echo. 
echo Skript wurde ausgefuehrt am:  
date /t 
time /t 

goto :EOF


::Sprungmarke PRINTSTOP, schreibt den Abschnitt der zu stoppenden Dienste in die log
:PRINTSTOP
::Print in Log
echo. 1>> %log% 2>>&1
echo %trenn2% 1>> %log% 2>>&1
echo Dienste stoppen 1>> %log% 2>>&1
echo %trenn2% 1>> %log% 2>>&1

::Print auf Konsole
echo. 
echo %trenn2% 
echo Dienste stoppen 
echo %trenn2% 
goto :EOF


::Sprungmarke PRINTSTART, schreibt den Abschnitt der zu startenden Dienste in die log
:PRINTSTART
::Print in Log
echo. 1>> %log% 2>>&1
echo %trenn2% 1>> %log% 2>>&1
echo Dienste starten 1>> %log% 2>>&1
echo %trenn2% 1>> %log% 2>>&1

::Print auf Konsole
echo. 
echo %trenn2% 
echo Dienste starten 
echo %trenn2% 
goto :EOF


::Sprungmarke STOPSERVICE, stoppt den �bermittelten Dienst
:STOPSERVICE
::Print in Log
echo. 1>> %log% 2>>&1
echo %1 wid gestoppt 1>> %log% 2>>&1
net stop %1 1>> %log% 2>>&1
echo %trenn3% 1>> %log% 2>>&1

::Print auf Konsole
echo. 
echo %1 wid gestoppt 
echo %trenn3% 
goto :EOF


::Sprungmarke STARTSERVICE, startet den �bermittelten Dienst
:STARTSERVICE
::Print in Log
echo. 1>> %log% 2>>&1
echo %1 wid gestartet 1>> %log% 2>>&1
net start %1 1>> %log% 2>>&1
echo %trenn3% 1>> %log% 2>>&1

::Print auf Konsole
echo. 
echo %1 wid gestartet 
echo %trenn3% 
goto :EOF


::Sprungmarke PRINTERROR, schreibt den error in die log
:PRINTERROR
::Print in Log
echo. 1>> %log% 2>>&1
echo %trenn3% 1>> %log% 2>>&1
echo ACHTUNG: %1 auf  %COMPUTERNAME% 1>> %log% 2>>&1
date /t 1>> %log% 2>>&1
time /t 1>> %log% 2>>&1
sc query %1 | find /i "state" 1>> %log% 2>>&1
echo %trenn3% 1>> %log% 2>>&1

::Print auf Konsole
echo. 
echo %trenn3% 
echo ACHTUNG: %1 auf  %COMPUTERNAME% 
date /t 
time /t 
sc query %1 | find /i "state" 
echo %trenn3% 
goto :EOF


::Sprungmarke MAIL versendet Fehlermeldung mit �bergebenem Parameter an mailingliste
::Hierzu wird das Programm blat.exe hinzugezogen
:MAIL
for /F "eol=; tokens=1" %%l in (%mailingliste%) do (
echo Mailversand an %%l
echo.
call blat.exe %log% -server %mailserver% -to %%l -f administrator@kl-it-consult.de -s "ACHTUNG: Fehler bei Dienst %1 auf %COMPUTERNAME%">NUL
)
goto :EOF


::Sprungmarke WARTEN bewirkt eine Pause von 'wartezeit' Sekunden
:WARTEN
timeout /T %wartezeit% /nobreak
goto :EOF


::Sprungmarke Ende, um aus dem Programm zu springen
:ENDE