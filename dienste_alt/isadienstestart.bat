@echo off

set log=%0_log.txt

echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 1>> %log% 2>>&1
echo. 1>> %log% 2>>&1
echo Skript wurde ausgefuehrt am:  1>> %log% 2>>&1
date /t 1>> %log% 2>>&1
time /t 1>> %log% 2>>&1
echo. 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo -------------------------------------------------------------------- 1>> %log% 2>>&1
echo Dienste starten 1>> %log% 2>>&1
echo -------------------------------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo Microsoft Firewall wid gestartet 1>> %log% 2>>&1
echo Microsoft Firewall wid gestartet
net start fwsrv 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo Routing und RAS wird gestartet 1>> %log% 2>>&1
echo Routing und RAS wird gestartet
net start RemoteAccess 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo IIS Verwaltungsdienst wird gestartet 1>> %log% 2>>&1
echo IIS Verwaltungsdienst wird gestartet
net start IISADMIN /y 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo WWW-Publishingdienst wird gestartet 1>> %log% 2>>&1
echo WWW-Publishingdienst wird gestartet
net start W3SVC 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo FTP-Publishingdienst wird gestartet 1>> %log% 2>>&1
echo FTP-Publishingdienst wird gestartet
net start MSFtpsvc 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo Windows Remote Managemant (WS-Management) wird gestartet 1>> %log% 2>>&1
echo Windows Remote Managemant (WS-Management) wird gestartet
net start WinRM 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo HTTP-SSL wird gestartet 1>> %log% 2>>&1
echo HTTP-SSL wird gestartet
net start HTTPFilter 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1
echo.
echo Siehe logfile fuer weitere Informationen: %log%
pause