@echo off

REM Dieses Skript startet die f�r den ISA relevanten Dienste (f�r IIS, OWA, VPN, ...) in der ben�tigten Reihenfolge neu.
REM Es sollte nat�rlich auf dem ISA ausgef�hrt werden.
REM Wird der ISA neugestartet, z.B. f�r Updates oder unerwartet, so starten die Dienste nach dem Neustart nicht in der richtigen Reihenfolge.
REM Dieses Skript sollte also nach Neustart des ISA ausgef�hrt werden.
REM ACHTUNG: Das Skript muss mit den entsprechenden Rechten ausgef�hrt werden.
REM Ablauf des Skriptes:
REM Die Dienste werden erst gestoppt und dann in der korrekten Reihenfolge gestartet,
REM danach wird der Status der Dienste alle X Sekunden in einer Endlosschleife gepr�ft
REM Falls ein Dienst wieder ausfallen sollte, werden die Dienste wieder gestartet.
REM ~~~~~~~~~~~~~~~~~~~A Script by Marwin Rieger~~~~~~~~~~~~~~~~~~~


REM Konstanten
set log=D:\log\isadienste_log.txt
set wartezeit=120

goto NEUSTARTEN

:START

timeout /T %wartezeit% /nobreak>NUL

net start | find /i "Microsoft-Firewall">NUL
if errorlevel==1 goto NEUSTARTEN

net start | find /i "Routing und RAS">NUL
if errorlevel==1 goto NEUSTARTEN

net start | find /i "IIS Verwaltungsdienst">NUL
if errorlevel==1 goto NEUSTARTEN

net start | find /i "FTP-Publishingdienst">NUL
if errorlevel==1 goto NEUSTARTEN

net start | find /i "HTTP-SSL">NUL
if errorlevel==1 goto NEUSTARTEN

net start | find /i "Windows Remote Management (WS-Management)">NUL
if errorlevel==1 goto NEUSTARTEN

net start | find /i "WWW-Publishingdienst">NUL
if errorlevel==1 goto NEUSTARTEN

goto START


:NEUSTARTEN
echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 1>> %log% 2>>&1
echo. 1>> %log% 2>>&1
echo Skript wurde ausgefuehrt am:  1>> %log% 2>>&1
date /t 1>> %log% 2>>&1
time /t 1>> %log% 2>>&1
echo. 1>> %log% 2>>&1
echo -------------------------------------------------------------------- 1>> %log% 2>>&1
echo Dienste stoppen 1>> %log% 2>>&1
echo -------------------------------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo Microsoft Firewall wid gestoppt 1>> %log% 2>>&1
net stop fwsrv /y 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo Routing und RAS wird gestoppt 1>> %log% 2>>&1
net stop RemoteAccess 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo IIS Verwaltungsdienst wird gestoppt 1>> %log% 2>>&1
net stop IISADMIN /y 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo FTP-Publishingdienst wird gestoppt 1>> %log% 2>>&1
net stop MSFtpsvc 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo HTTP-SSL wird gestoppt 1>> %log% 2>>&1
net stop HTTPFilter 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo Windows Remote Managemant (WS-Management) wird gestoppt 1>> %log% 2>>&1
net stop WinRM 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo WWW-Publishingdienst wird gestoppt 1>> %log% 2>>&1
net stop W3SVC 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo -------------------------------------------------------------------- 1>> %log% 2>>&1
echo Dienste starten 1>> %log% 2>>&1
echo -------------------------------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo Microsoft Firewall wid gestartet 1>> %log% 2>>&1
net start fwsrv 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo Routing und RAS wird gestartet 1>> %log% 2>>&1
net start RemoteAccess 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo IIS Verwaltungsdienst wird gestartet 1>> %log% 2>>&1
net start IISADMIN /y 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo WWW-Publishingdienst wird gestartet 1>> %log% 2>>&1
net start W3SVC 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo FTP-Publishingdienst wird gestartet 1>> %log% 2>>&1
net start MSFtpsvc 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo Windows Remote Managemant (WS-Management) wird gestartet 1>> %log% 2>>&1
net start WinRM 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo HTTP-SSL wird gestartet 1>> %log% 2>>&1
net start HTTPFilter 1>> %log% 2>>&1

goto START