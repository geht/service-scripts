@echo off

set log=%0_log.txt

echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 1>> %log% 2>>&1
echo. 1>> %log% 2>>&1
echo Skript wurde ausgefuehrt am:  1>> %log% 2>>&1
date /t 1>> %log% 2>>&1
time /t 1>> %log% 2>>&1
echo. 1>> %log% 2>>&1
echo -------------------------------------------------------------------- 1>> %log% 2>>&1
echo Dienste stoppen 1>> %log% 2>>&1
echo -------------------------------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo Microsoft Firewall wid gestoppt 1>> %log% 2>>&1
echo Microsoft Firewall wid gestoppt
net stop fwsrv /y 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo Routing und RAS wird gestoppt 1>> %log% 2>>&1
echo Routing und RAS wird gestoppt
net stop RemoteAccess 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo IIS Verwaltungsdienst wird gestoppt 1>> %log% 2>>&1
echo IIS Verwaltungsdienst wird gestoppt
net stop IISADMIN /y 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo FTP-Publishingdienst wird gestoppt 1>> %log% 2>>&1
echo FTP-Publishingdienst wird gestoppt
net stop MSFtpsvc 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo HTTP-SSL wird gestoppt 1>> %log% 2>>&1
echo HTTP-SSL wird gestoppt
net stop HTTPFilter 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo Windows Remote Managemant (WS-Management) wird gestoppt 1>> %log% 2>>&1
echo Windows Remote Managemant (WS-Management) wird gestoppt
net stop WinRM 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1

echo. 1>> %log% 2>>&1
echo WWW-Publishingdienst wird gestoppt 1>> %log% 2>>&1
echo WWW-Publishingdienst wird gestoppt
net stop W3SVC 1>> %log% 2>>&1
echo --------------------------------------------- 1>> %log% 2>>&1
echo.
echo Siehe logfile fuer weitere Informationen: %log%
pause